;Author: Hussain Parsaiyan
;Copyright: Hussain Parsaiyan
;License: GPLv3

org 100h

    mov         ah,9h
    lea         dx,first_msg
    int         21h
    pusha
    lea         si,string_entered

start:

    mov         ah,1
    int         21h
    cmp         al,97
    jb          storing_string
    cmp         al, 122
    ja          storing_string
    add         al,-32

storing_string:

    cmp         al, 13
    je          here
    mov         [si],al
    inc         si
    jmp         start

here:

    mov         [si],36
    lea         si,string_entered
    cmp         [si],36
    je          checked
    popa
    pusha

checking:

    lea         bx,string_entered
    lea         cx,repeats
    mov         si,bx
    inc         si
    jmp         check_loop

inc_bx:

    inc         bx
    mov         si,bx
    inc         si

check_loop:

    cmp         [bx],36
    je          checked
    cmp         [bx],0
    je          inc_bx
    cmp         [si],36
    je          inc_bx
    mov         al,[si]
    cmp         [bx],al
    je          store_to_repeat
    inc         si
    jmp         check_loop

store_to_repeat:

    pusha

blah:

    mov         [si],0
    inc         si

check_for_other_repeats:

    cmp         [si],36
    je          storing
    mov         al,[si]
    cmp         [bx],al
    je          blah
    inc         si
    jmp         check_for_other_repeats

storing:

    popa
    mov         al, [bx]
    cmp         al,32
    je          space_found
    push        bx
    mov         bx,cx
    mov         [bx], al; can't use cx as pointer
    mov         al,44
    inc         bx
    mov         [bx],al
    pop         bx
    inc         cx
    inc         cx

space_found:

    inc         bx
    mov         si,bx
    inc         si
    jmp         check_loop

checked:

    mov         bx,cx
    mov         [bx],36
    popa

printing_repeats:

    mov         ah,9h
    lea         dx,last_msg
    int         21h
    lea         dx,repeats
    int         21h
    lea         dx,exit_stat
    int         21h
    mov         ah,00
    int         16h

ret

repeats         db 50 dup (?)
string_entered  db 50 dup (?)
first_msg       db 'Please enter a statement/word of your own choice and hit enter :',10,10,13,'$'
last_msg        db 10,10,13,'The following character(s) are common in your statement/word:',10,13,'$'
exit_stat       db 10,10,13,'Press any key to exit$'